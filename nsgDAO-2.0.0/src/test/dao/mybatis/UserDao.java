package test.dao.mybatis;


import global.Cache;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nsg.dao.TransIsoLevel;
import org.nsg.dao.Transaction;
import org.nsg.dao.mybatis.MyBatisSessionMgr;

import test.dao.mybatis.vo.Interest;
import test.dao.mybatis.vo.User;
import test.dao.mybatis.vo.mapper.UserMapper;

public class UserDao extends MyBatisBaseDao
{
	Logger logger = LogManager.getLogger(UserDao.class);
	
	public UserDao()
	{
		super();
	}
	
	protected UserDao(MyBatisSessionMgr mgr)
	{
		super(mgr);
	}
	
	@Transaction(level=TransIsoLevel.REPEATABLE_READ)
	public boolean createUser(User user)
	{		
		UserMapper mapper = getMapper(UserMapper.class);
		int effect = mapper.createUser(user);
		
		if(effect > 0)
		{
			changeSessionExecutorTypeToBatch();

			int userId = user.getId();
			List<Integer> interests = user.getInterests();
			
			if(interests != null)
				for(Integer interestId : interests)
					mapper.createUserInterest(userId, interestId);	
		}
		logger.info(this.getClass().getName()+" ---> "+"UserDao.createUser()");
		return (effect > 0);
	}

	public boolean deleteUser(int id)
	{
		UserMapper mapper = getMapper(UserMapper.class);
		mapper.deleteUserInterest(id);
		int effect = mapper.deleteUser(id);
		
		return (effect > 0);
	}

	@Transaction(false)
	public List<User> findUsers(String name, int experience)
	{
		Cache cache			= Cache.getInstance();
		UserMapper mapper	= getMapper(UserMapper.class);
		List<User> users	= mapper.findUsers(name, experience);
		
		for(User user : users)
		{			
			user.setGenderObj(cache.getGenderById(user.getGender()));
			user.setExperienceObj(cache.getExperenceById(user.getExperience()));
			
			List<Integer> ints = user.getInterests();
			
			if(ints != null)
			{
				List<Interest> intsObjs = new ArrayList<Interest>();
				
				for(Integer i : ints)
					intsObjs.add(cache.getInterestById(i));
				
				user.setInterestObjs(intsObjs);
			}
		}
		
		return users;
	}

	@Transaction(false)
	public List<User> queryInterest(int gender, int experience)
	{
		Cache cache			= Cache.getInstance();
		UserMapper mapper	= getMapper(UserMapper.class);
		List<User> users	= mapper.queryInterest(gender, experience);
		
		for(User user : users)
		{			
			user.setGenderObj(cache.getGenderById(user.getGender()));
			user.setExperienceObj(cache.getExperenceById(user.getExperience()));
			
			List<Integer> ints = user.getInterests();
			
			if(ints != null)
			{
				List<Interest> intsObjs = new ArrayList<Interest>();
				
				for(Integer i : ints)
					intsObjs.add(cache.getInterestById(i));
				
				user.setInterestObjs(intsObjs);
			}
		}
		
		return users;
	}
}
