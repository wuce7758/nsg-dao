package test.dao.mybatis.vo;

public class Interest extends SimpleVO
{
	public Interest()
	{
		super();
	}
	
	public Interest(int id, String name)
	{
		super(id, name);
	}
}
