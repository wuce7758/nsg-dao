package test.dao.mybatis.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.nsg.util.LogUtil;

import test.dao.mybatis.UserDao;
import test.dao.mybatis.vo.User;

/**
 * Servlet implementation class MybatisTest
 */
@WebServlet("/MybatisTest")
public class MybatisTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MybatisTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("MybatisTest.doGet()");
//		User user = new User();
//		user.setName("Jeff111");
//		user.setAge(111);
//		user.setGender(121);
//		user.setExperience(131);
//		System.out.println(user.getAge());
//		UserDao dao = new UserDao();
//		dao.createUser(user);
//		//LogUtil.exception(new RuntimeException(), dao, Level.ERROR, false);
		
		UserDao dao = new UserDao();
		dao.deleteUser(83);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
