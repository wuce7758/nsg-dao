package test.dao.mybatis;

import global.MyConfigParser;

import org.nsg.dao.mybatis.MyBatisFacade;
import org.nsg.dao.mybatis.MyBatisSessionMgr;

/**
 * @Description  MyBatisBaseDao.java
 *
 * @author 泥沙砖瓦浆木匠
 * @date 2014年10月29日下午8:54:17
 * @version 1.0
 */
public class MyBatisBaseDao extends MyBatisFacade
{
	protected MyBatisBaseDao()
	{
		this(MyConfigParser.getMyBaitsSessionMgr());
	}

	protected MyBatisBaseDao(MyBatisSessionMgr mgr)
	{
		super(mgr);
	}

}
