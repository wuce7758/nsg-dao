package org.nsg.dao;

/**
 * @Description  DAOException.java
 *	数据库访问相关异常（运行期异常）
 * @author 泥沙砖瓦浆木匠
 * @date 2014年10月29日下午6:51:40
 * @version 1.0
 */
@SuppressWarnings("serial")
public class DAOException extends RuntimeException
{
	public DAOException()
	{
	}
	
	public DAOException(String desc)
	{
		super(desc);
	}
	
	public DAOException(Throwable e)
	{
		super(e);
	}
	
	public DAOException(String desc,Throwable e)
	{
		super(desc,e);
	}
}
