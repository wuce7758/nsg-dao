package org.nsg.dao.mybatis;

import org.apache.ibatis.session.SqlSession;
import org.nsg.dao.CustomTransaction;

/**
 * @Description  MyBatisTransaction.java
 *
 * @author 泥沙砖瓦浆木匠
 * @date 2014年10月29日下午8:20:50
 * @version 1.0
 */
public interface MyBatisTransaction extends CustomTransaction<MyBatisSessionMgr, SqlSession>
{

}
