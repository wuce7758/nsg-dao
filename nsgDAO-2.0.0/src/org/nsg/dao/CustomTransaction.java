package org.nsg.dao;

/**
 * @Description  CustomTransaction.java
 *	自定义事务
 *
 * @param <S>	: 数据库连接类型
 * @param <M>	: 数据库连接管理器类型
 * @author 泥沙砖瓦浆木匠
 * @date 2014年10月29日下午6:49:40
 * @version 1.0
 */
public interface CustomTransaction<M extends SessionMgr<S>,S>
{
	/**
	 * 自定义事务入口方法
	 */
	void execute(M mgr) throws DAOException;
}
