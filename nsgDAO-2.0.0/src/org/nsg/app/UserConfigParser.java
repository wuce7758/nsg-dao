package org.nsg.app;

import org.dom4j.Element;

/**
 * 
 * @Description 
 *	Interface to parser user configuration information
 * @author nsg
 * @date 22014/10/28 20:48:40
 * @version 1.0
 */
public interface UserConfigParser
{
	/** 
	 * begin to parser （begin from the node: '&lt;user&gt;'）
	 * 
	 *  @param user	: app-config.xml 's node:  '&lt;user&gt;' 
	 */
	void parse(Element user);
}
